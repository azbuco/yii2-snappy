<?php

namespace azbuco\snappy;

use Exception;
use Knp\Snappy\Pdf;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\ServerErrorHttpException;

class ImageConverter extends BaseConverter {

    public function convert($html, $options = [])
    {
        $options = ArrayHelper::merge($this->options, $options);

        if ($this->bin === null) {
            try {
                $this->bin = $this->guessBin();
            } catch (Exception $ex) {
                throw new ServerErrorHttpException('PdfConverter is unable to detect the wkhtmltoimage binary, please explicitly set it.');
            }
        }

        $snappy = new \Knp\Snappy\Image($this->bin);

        return $snappy->getOutputFromHtml($html, $options);
    }

    public function guessBin()
    {
        $os = $this->os();
        $architecture = $this->architecture();

        if ($os === self::OS_WINDOWS && $architecture === self::ARCHITECTURE_32) {
            $bin = '@vendor/wemersonjanuario/wkhtmltopdf-windows/bin/32bit/wkhtmltoimage.exe';
        } else if ($os === self::OS_WINDOWS && $architecture === self::ARCHITECTURE_64) {
            $bin = '@vendor/wemersonjanuario/wkhtmltopdf-windows/bin/64bit/wkhtmltoimage.exe';
        } else if ($architecture === self::ARCHITECTURE_32) {
            $bin = '@vendor/h4cc/wkhtmltopdf-i386/bin/wkhtmltoimage-i386';
        } else {
            $bin = '@vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltoimage-amd64';
        }

        return Yii::getAlias($bin);
    }

}

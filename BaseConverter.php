<?php

namespace azbuco\snappy;

use yii\base\Component;

class BaseConverter extends Component {

    const OS_WINDOWS = 'windows';
    const OS_LINUX = 'linux';
    const ARCHITECTURE_32 = 'i386';
    const ARCHITECTURE_64 = 'amd64';

    /**
     * @var array default command line options
     */
    public $options = [];
    
    /**
     * @var string path to the wkhtmltopdf binary
     * If null, the converter tries to guess the proper binary
     */
    public $bin;

    public function os()
    {
        return strtoupper(substr(PHP_OS, 0, 3)) === 'WIN' ? self::OS_WINDOWS : self::OS_LINUX;
    }

    public function architecture()
    {
        return PHP_INT_SIZE == 4 ? self::ARCHITECTURE_32 : self::ARCHITECTURE_64;
    }
    
}

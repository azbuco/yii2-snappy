<?php

namespace azbuco\snappy;

use Yii;
use yii\base\Event;
use yii\web\Response;

class PdfResponseFormatter extends BaseFormatter {

    const FORMAT_PDF = 'pdf';

    public $contentType = 'application/pdf';
    
    public function init()
    {
        parent::init();

        if ($this->converter === null) {
            $this->converter = Yii::createObject(PdfConverter::class);
        }

//        // az alábbi funkció nagyon hasznos lenne, hogy a pdf-ből kivegye a nem bele való script tageket,
//        // de sajnos a DOMDocument loadHTML a legkisebb formai hiba esetén is hibát dob, így vagy nagyon
//        // kéne vigyázni a kimenetre vagy nem használjuk.
//
//        \Yii::$app->response->on(Response::EVENT_BEFORE_SEND, function(Event $event) {
//            $response = $event->sender;
//            /* @var $response \yii\web\Response */
//            if ($response->format === self::FORMAT_PDF) {
//                $dom = new \DOMDocument();
//                $dom->loadHTML($response->data);
//
//                while (($r = $dom->getElementsByTagName("script")) && $r->length) {
//                    $r->item(0)->parentNode->removeChild($r->item(0));
//                }
//
//                $response->data = $dom->saveHTML();
//            }
//        });
    }

}

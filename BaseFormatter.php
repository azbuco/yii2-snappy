<?php

namespace azbuco\snappy;

use Yii;
use yii\base\Component;
use yii\web\Response;
use yii\web\ResponseFormatterInterface;

class BaseFormatter extends Component implements ResponseFormatterInterface {

    public $converter;
    public $contentType;
    public $fileName;
    public $options = [];

    /**
     * Formats the specified response.
     * @param Response $response the response to be formatted.
     */
    public function format($response)
    {
        $response->getHeaders()->set('Content-Type', $this->contentType);
        if ($this->fileName !== null) {
            $response->getHeaders()->set('Content-Disposition', "attachment; filename=\"{$this->fileName}\"");
        }

        $converter = $this->converter();

        $response->content = $converter->convert($response->data, $this->options);
    }

    public function converter()
    {
        // converter defined explicitly
        if ($this->converter instanceof BaseConverter) {
            return $this->converter;
        }

        // configuration object
        try {
            $obj = Yii::createObject($this->converter);
        } catch (\Exception $ex) {
            throw new \yii\base\InvalidConfigException('The converter property must be an instance of BaseConverter, or a configuration object');
        }

        return $obj;
    }

}

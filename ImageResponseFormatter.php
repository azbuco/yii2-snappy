<?php

namespace azbuco\snappy;

class ImageResponseFormatter extends BaseFormatter {

    public $contentType = 'image/jpeg';
    
    public function init()
    {
        parent::init();
        if ($this->converter === null) {
            $this->converter = new ImageConverter();
        }
    }

}

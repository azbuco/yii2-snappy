# Yii2 Snappy #

Yii2 wrapper for Snappy (wrapper wkhtmltopdf and wkhtmltoimage)

## Usage

### Via config

If you want to use the same pdf configuration application wide, configure the 
response application component like the following (usually config/main.php):

```
// ...
'response' => [
	'formatters' => [
		// ...
		'pdf' => [
			'class' => 'azbuco\snappy\PdfResponseFormatter',
				// if you want to open the PDF in browser, just comment the line below
				'fileName' => 'foo.php',
				// wkhtmltopdf config options, see: https://wkhtmltopdf.org/usage/wkhtmltopdf.txt
				'options' => [
					'orientation' => 'Portrait',
					'footer-right' => '[page] / [toPage] oldal',
					'footer-font-size' => 7,
				],
			],
		],
	],
],
```

And use in your action:

```
// ...
public function pdfAction() {
	\Yii::$app->response->format = 'pdf';
	return '<h1 style="text-align: center;">Hurray, a PDF</h1>';
}
```

### Create your formatter on the fly

If you only need the formatter once, or you want to configure the formatter 
with specific settings, use like this:

```
// ...
public function pdfAction() {
	$formatter = new \azbuco\snappy\PdfResponseFormatter([
		'options' => [
			'orientation' => 'Landscape',
			'footer-right' => '[page] / [toPage] oldal',
			'footer-font-size' => 7,
		],
		'fileName' => 'whatever.pdf'
   	]);
				
	\Yii::$app->response->formatters['pdf'] = $formatter;
    \Yii::$app->response->format = 'pdf';
				
	return '<h1 style="text-align: center;">Hurray, a PDF</h1>';
}
```

### Notes

If you have scripts or css files linked in the content, they may not work as relative path, 
in this case add the base html tag to your layout.

```
<!DOCTYPE html>
<html>
	<head>
		<base href="<?= Url::base(true) ?>" target="_blank">
		<!-- ... -->
```

It's wkhtmltopdf problem, but with multipage HTML tables this css resolves the collapsed 
table headers problem:

```
<style>
table { 
	overflow: visible !important; 
}
thead { 
	display: table-header-group; 
	background: #ededed; 
	border-bottom: 1px solid #717171;
}
tfoot { 
	/* display: table-row-group; */
}
tr, td, th { 
	page-break-inside: avoid
}
</style>
```



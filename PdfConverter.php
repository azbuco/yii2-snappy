<?php

namespace azbuco\snappy;

use Exception;
use Knp\Snappy\Pdf;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\ServerErrorHttpException;

class PdfConverter extends BaseConverter {

    const WIN32_BIN = '@vendor/wemersonjanuario/wkhtmltopdf-windows/bin/32bit/wkhtmltopdf.exe';
    const WIN64_BIN = '@vendor/wemersonjanuario/wkhtmltopdf-windows/bin/64bit/wkhtmltopdf.exe';
    const LIN32_BIN = '@vendor/h4cc/wkhtmltopdf-i386/bin/wkhtmltopdf-i386';
    const LIN64_BIN = '@vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64';

    public function convert($html, $options = [])
    {
        $options = ArrayHelper::merge($this->options, $options);

        if ($this->bin === null) {
            try {
                $this->bin = realpath($this->guessBin());
            } catch (Exception $ex) {
                throw new ServerErrorHttpException('PdfConverter is unable to detect the wkhtmltopdf binary, please explicitly set it.');
            }
        }

        $snappy = new Pdf($this->bin);
        $snappy->setTemporaryFolder(Yii::getAlias('@runtime'));

        return $snappy->getOutputFromHtml($html, $options);
    }

    public function guessBin()
    {
        $os = $this->os();
        $architecture = $this->architecture();

        if ($os === self::OS_WINDOWS && $architecture === self::ARCHITECTURE_32) {
            $bin = self::WIN32_BIN;
        } else if ($os === self::OS_WINDOWS && $architecture === self::ARCHITECTURE_64) {
            $bin = self::WIN64_BIN;
        } else if ($architecture === self::ARCHITECTURE_32) {
            $bin = self::LIN32_BIN;
        } else {
            $bin = self::LIN64_BIN;
        }

        return Yii::getAlias($bin);
    }

}
